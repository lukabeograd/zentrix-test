import Uuid from "../util/Uuid";
import type { Nullable } from "../util/types/nullable";

export default class Contact {
  public id: string;
  public userId: string;
  public name: string;
  public homePhone: Nullable<string>;
  public workPhone: Nullable<string>;
  public email: Nullable<string>;
  public address: Nullable<string>;
  public createdAt: Date;
  public updatedAt: Date;

  constructor(
    id: string,
    userId: string,
    name: string,
    homePhone: Nullable<string>,
    workPhone: Nullable<string>,
    email: Nullable<string>,
    address: Nullable<string>,
    createdAt: Date,
    updatedAt: Date
  ) {
    this.id = id;
    this.userId = userId;
    this.name = name;
    this.homePhone = homePhone;
    this.workPhone = workPhone;
    this.email = email;
    this.address = address;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public static create(
    userId: string,
    name: string,
    homePhone: Nullable<string> = null,
    workPhone: Nullable<string> = null,
    email: Nullable<string> = null,
    address: Nullable<string> = null
  ): Contact {
    return new Contact(
      Uuid.create(),
      userId,
      name,
      homePhone,
      workPhone,
      email,
      address,
      new Date(),
      new Date()
    );
  }
}
