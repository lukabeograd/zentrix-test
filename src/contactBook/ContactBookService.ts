import ListOptions from "../util/ListOptions";
import Contact from "./Contact";
import ContactBookRepository from "./ContactBookRepository";
import ContactNotFoundError from "./errors/ContactNotFoundError";

export default class ContactBookService {
  private contactBookRepository: ContactBookRepository;

  constructor(contactBookRepository: ContactBookRepository) {
    this.contactBookRepository = contactBookRepository;
  }

  public async createContact(contact: Contact): Promise<void> {
    return this.contactBookRepository.save(contact);
  }

  public async getContact(id: string): Promise<Contact> {
    const contact = await this.contactBookRepository.findById(id);
    if (!contact) {
      throw new ContactNotFoundError();
    }

    return contact;
  }

  public async deleteContact(id: string): Promise<void> {
    const contact = await this.getContact(id);

    await this.contactBookRepository.delete(contact.id);
  }

  public async updateContact(
    id: string,
    newContact: Contact
  ): Promise<Contact> {
    const contact = await this.getContact(id);

    contact.name = newContact.name;
    contact.homePhone = newContact.homePhone;
    contact.workPhone = newContact.workPhone;
    contact.email = newContact.email;
    contact.address = newContact.address;
    contact.updatedAt = new Date();

    await this.contactBookRepository.update(contact);

    return contact;
  }

  public async list(userId: string, options: ListOptions): Promise<Contact[]> {
    return this.contactBookRepository.listByUserId(userId, options);
  }
}
