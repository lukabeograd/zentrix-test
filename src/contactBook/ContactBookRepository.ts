import ListOptions from "../util/ListOptions";
import Contact from "./Contact";
import type { Nullable } from "../util/types/nullable";

export default interface ContactBookRepository {
  save(contact: Contact): Promise<void>;
  listByUserId(userId: string, options: ListOptions): Promise<Contact[]>;
  findById(id: string): Promise<Nullable<Contact>>;
  delete(id: string): Promise<void>;
  update(contact: Contact): Promise<void>;
}
