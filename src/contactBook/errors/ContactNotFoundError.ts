export default class ContactNotFoundError extends Error {
  constructor() {
    super("Contact not found");
  }
}
