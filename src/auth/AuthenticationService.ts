import User from "../user/User";
import UserService from "../user/UserService";
import AuthenticationRepository from "./AuthenticationRepository";
import InvalidCredentialsError from "./errors/InvalidCredentialsError";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export default class AuthenticationService {
  private authRepository: AuthenticationRepository;
  private userService: UserService;

  constructor(
    authRepository: AuthenticationRepository,
    userService: UserService
  ) {
    this.authRepository = authRepository;
    this.userService = userService;
  }

  public async login(email: string, password: string): Promise<string> {
    try {
      const user = await this.userService.findByEmail(email);
      if (!user) {
        throw new Error("User not found");
      }

      const encryptedPassDb = await this.authRepository.findByUserId(user.id);
      if (!encryptedPassDb) {
        throw new InvalidCredentialsError();
      }

      const isPasswordValid = await bcrypt.compare(password, encryptedPassDb);
      if (!isPasswordValid) {
        throw new InvalidCredentialsError();
      }

      const token = this.generateToken(user);

      return token;
    } catch (error) {
      throw new InvalidCredentialsError();
    }
  }

  public async register(user: User, password: string): Promise<string> {
    const [existingEmail, existingUsername] = await Promise.all([
      this.userService.findByEmail(user.email),
      this.userService.findByUsername(user.username),
    ]);

    if (existingEmail || existingUsername) {
      throw new Error("User already exists");
    }

    await this.userService.save(user);

    const hashedPassword = await bcrypt.hash(password, 10);
    await this.authRepository.register(user, hashedPassword);

    const token = this.generateToken(user);

    return token;
  }

  private generateToken(user: User): string {
    return jwt.sign({ userId: user.id }, process.env.JWT_SECRET || "");
  }
}
