import User from "../user/User";
import type { Nullable } from "../util/types/nullable";

export default interface AuthenticationRepository {
  findByUserId(userId: string): Promise<Nullable<string>>;
  register(user: User, password: string): Promise<void>;
}
