import Uuid from "../util/Uuid";

export default class User {
  public id: string;
  public username: string;
  public email: string;

  public createdAt: Date;

  constructor(id: string, username: string, email: string, createdAt: Date) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.createdAt = createdAt;
  }

  public static create(username: string, email: string): User {
    return new User(Uuid.create(), username, email, new Date());
  }
}
