import User from "./User";
import UserRepository from "./UserRepository";
import type { Nullable } from "../util/types/nullable";

export default class UserService {
  private userRepository: UserRepository;
  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }

  public async findByEmail(email: string): Promise<Nullable<User>> {
    return this.userRepository.findByEmail(email);
  }

  public async findByUsername(username: string): Promise<Nullable<User>> {
    return this.userRepository.findByUsername(username);
  }

  public async save(user: User): Promise<void> {
    return this.userRepository.save(user);
  }
}
