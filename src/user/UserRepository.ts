import User from "./User";
import type { Nullable } from "../util/types/nullable";

export default interface UserRepository {
  findByEmail(email: string): Promise<Nullable<User>>;
  findByUsername(username: string): Promise<Nullable<User>>;
  save(user: User): Promise<void>;
}
