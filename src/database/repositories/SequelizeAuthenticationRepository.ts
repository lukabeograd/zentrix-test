import AuthenticationRepository from "../../auth/AuthenticationRepository";
import User from "../../user/User";
import PasswordModel from "../models/PasswordModel";
import type { Nullable } from "../../util/types/nullable";

export default class SequelizeAuthenticationRepository
  implements AuthenticationRepository
{
  public async findByUserId(userId: string): Promise<Nullable<string>> {
    const password = await PasswordModel.findOne({ where: { userId } });
    if (!password) {
      return null;
    }

    return password.encryptedPassword;
  }

  public async register(user: User, password: string): Promise<void> {
    await PasswordModel.create({
      userId: user.id,
      encryptedPassword: password,
    });
  }
}
