import User from "../../user/User";
import UserRepository from "../../user/UserRepository";
import UserModel from "../models/UserModel";
import type { Nullable } from "../../util/types/nullable";

export default class SequelizeUserRepository implements UserRepository {
  public async save(user: User): Promise<void> {
    await UserModel.create({
      id: user.id,
      username: user.username,
      email: user.email,
      createdAt: user.createdAt,
    });
  }

  public async findByEmail(email: string): Promise<Nullable<User>> {
    const dbUser = await UserModel.findOne({ where: { email } });
    if (!dbUser) {
      return null;
    }

    return this.toUser(dbUser);
  }

  public async findByUsername(username: string): Promise<Nullable<User>> {
    const dbUser = await UserModel.findOne({ where: { username } });
    if (!dbUser) {
      return null;
    }

    return this.toUser(dbUser);
  }

  private toUser(dbUser: UserModel): User {
    return new User(dbUser.id, dbUser.username, dbUser.email, dbUser.createdAt);
  }
}
