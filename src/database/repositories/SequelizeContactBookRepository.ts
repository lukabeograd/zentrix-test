import Contact from "../../contactBook/Contact";
import ContactBookRepository from "../../contactBook/ContactBookRepository";
import ListOptions from "../../util/ListOptions";
import ContactModel from "../models/ContactModel";
import type { Nullable } from "../../util/types/nullable";

export default class SequelizeContactBookRepository
  implements ContactBookRepository
{
  public async save(contact: Contact): Promise<void> {
    await ContactModel.create({
      id: contact.id,
      userId: contact.userId,
      name: contact.name,
      homePhone: contact.homePhone,
      workPhone: contact.workPhone,
      email: contact.email,
      address: contact.address,
      createdAt: contact.createdAt,
      updatedAt: contact.updatedAt,
    });
  }

  public async listByUserId(
    userId: string,
    options: ListOptions
  ): Promise<Contact[]> {
    const limit = options.pageSize;
    const offset = options.pageSize * (options.page - 1);

    const whereClause: any = { userId };

    if (options.email) {
      whereClause.email = options.email;
    }
    if (options.name) {
      whereClause.name = options.name;
    }

    const contacts = await ContactModel.findAll({
      where: whereClause,
      limit: limit,
      offset: offset,
    });

    return contacts.map((contact) => this.toContact(contact));
  }

  public async findById(id: string): Promise<Nullable<Contact>> {
    const contact = await ContactModel.findOne({ where: { id } });
    if (!contact) {
      return null;
    }

    return this.toContact(contact);
  }

  public async delete(id: string): Promise<void> {
    await ContactModel.destroy({ where: { id } });
  }

  public async update(contact: Contact): Promise<void> {
    const dbContact = await ContactModel.findOne({ where: { id: contact.id } });
    if (!dbContact) {
      throw new Error("Contact not found");
    }

    await dbContact.update({
      name: contact.name,
      homePhone: contact.homePhone,
      workPhone: contact.workPhone,
      email: contact.email,
      address: contact.address,
      updatedAt: new Date(),
    });
  }

  private toContact(dbContact: ContactModel): Contact {
    return new Contact(
      dbContact.id,
      dbContact.userId,
      dbContact.name,
      dbContact.homePhone,
      dbContact.workPhone,
      dbContact.email,
      dbContact.address,
      dbContact.createdAt,
      dbContact.updatedAt
    );
  }
}
