import { Sequelize } from "sequelize";
import ConnectionConfiguration from "./ConnectionConfiguraton";
import UserModel from "./models/UserModel";
import PasswordModel from "./models/PasswordModel";
import ContactModel from "./models/ContactModel";

export default class SequelizeConnection {
  private static instance: Sequelize;

  private constructor() {}

  public static getInstance() {
    if (!SequelizeConnection.instance) {
      SequelizeConnection.instance = SequelizeConnection.buildConnection();
    }
    return SequelizeConnection.instance;
  }

  private static buildConnection(): Sequelize {
    const databaseUrl = ConnectionConfiguration.DATABASE_URL;
    if (!databaseUrl) {
      throw new Error("Database URL is not set");
    }

    return new Sequelize(databaseUrl);
  }

  public static initiateModels() {
    const instance = SequelizeConnection.getInstance();

    UserModel.initModel(instance);
    PasswordModel.initModel(instance);
    ContactModel.initModel(instance);
  }
}
