export type UserAttributes = {
  id: string;
  username: string;
  email: string;
  createdAt: Date;
};

export type PasswordAttributes = {
  id: string;
  userId: string;
  encryptedPassword: string;
  createdAt: Date;
};

type PasswordCreationAttributes = Optional<
  PasswordAttributes,
  "id" | "createdAt"
>;

export type ContactAttributes = {
  id: string;
  userId: string;
  name: string;
  homePhone: Nullable<string>;
  workPhone: Nullable<string>;
  email: Nullable<string>;
  address: Nullable<string>;
  createdAt: Date;
  updatedAt: Date;
};
