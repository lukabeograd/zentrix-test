import { DataTypes, Model, Sequelize } from "sequelize";
import type {
  PasswordAttributes,
  PasswordCreationAttributes,
} from "./attributes";

export default class PasswordModel extends Model<
  PasswordAttributes,
  PasswordCreationAttributes
> {
  public id!: string;
  public userId!: string;
  public encryptedPassword!: string;
  public createdAt!: Date;

  public static initModel(sequelize: Sequelize) {
    PasswordModel.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
        },
        userId: {
          type: DataTypes.UUID,
          allowNull: false,
          field: "user_id",
        },
        encryptedPassword: {
          type: DataTypes.STRING,
          allowNull: false,
          field: "encrypted_password",
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
          field: "created_at",
        },
      },
      { sequelize, tableName: "passwords", createdAt: true, updatedAt: false }
    );
  }
}
