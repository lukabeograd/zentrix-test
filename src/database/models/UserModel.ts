import { DataTypes, Model, Sequelize } from "sequelize";
import type { UserAttributes } from "./attributes";

export default class UserModel extends Model<UserAttributes> {
  public id!: string;
  public username!: string;
  public email!: string;
  public createdAt!: Date;

  public static initModel(sequelize: Sequelize) {
    UserModel.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
        },
        username: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
          field: "created_at",
        },
      },
      { sequelize, tableName: "users", createdAt: true, updatedAt: false }
    );
  }
}
