import { DataTypes, Model, Sequelize } from "sequelize";
import type { ContactAttributes } from "./attributes";

export default class ContactModel extends Model<ContactAttributes> {
  public id!: string;
  public userId!: string;
  public name!: string;
  public homePhone!: string | null;
  public workPhone!: string | null;
  public email!: string | null;
  public address!: string | null;
  public createdAt!: Date;
  public updatedAt!: Date;

  public static initModel(sequelize: Sequelize) {
    ContactModel.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
        },
        userId: {
          type: DataTypes.UUID,
          allowNull: false,
          field: "user_id",
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        homePhone: {
          type: DataTypes.STRING,
          allowNull: true,
          field: "home_phone",
        },
        workPhone: {
          type: DataTypes.STRING,
          allowNull: true,
          field: "work_phone",
        },
        email: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        address: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
          field: "created_at",
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW,
          field: "updated_at",
        },
      },
      { sequelize, tableName: "contacts", createdAt: true, updatedAt: true }
    );
  }
}
