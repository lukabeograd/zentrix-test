export default class WebServerConfig {
  public static readonly PORT: number = process.env.PORT
    ? parseInt(process.env.PORT)
    : 8080;
}
