import { Request, Response } from "express";
import AuthenticationService from "../../auth/AuthenticationService";
import User from "../../user/User";
import InvalidCredentialsError from "../../auth/errors/InvalidCredentialsError";
import Logger from "../../util/logger/Logger";
import type { Dependencies } from "../../util/types/dependencies";

export default class AuthController {
  private authService: AuthenticationService;
  private logger: Logger;

  constructor({ authenticationService, logger }: Dependencies) {
    this.authService = authenticationService;
    this.logger = logger;
  }

  public async signup(req: Request, res: Response) {
    const { username, email, password } = req.body;

    try {
      const user = User.create(username, email);

      const token = await this.authService.register(user, password);

      this.logger.info("User signed up", { username: user.username });

      return res.status(200).json({ token });
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  }

  public async login(req: Request, res: Response) {
    try {
      const token = await this.authService.login(
        req.body.email,
        req.body.password
      );

      return res.status(200).json({ token });
    } catch (error: any) {
      if (error instanceof InvalidCredentialsError) {
        return res.status(401).json({ error: error.message });
      }

      return res.status(400).json({ error: error.message });
    }
  }
}
