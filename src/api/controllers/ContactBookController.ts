import { Request, Response } from "express";
import Logger from "../../util/logger/Logger";
import type { Dependencies } from "../../util/types/dependencies";
import ContactBookService from "../../contactBook/ContactBookService";
import Contact from "../../contactBook/Contact";

export default class ContactBookController {
  private contactBookService: ContactBookService;
  private logger: Logger;

  constructor({ contactBookService, logger }: Dependencies) {
    this.contactBookService = contactBookService;
    this.logger = logger;
  }

  public async create(req: Request, res: Response) {
    const { userId, name, homePhone, workPhone, email, address } = req.body;

    try {
      const contact = Contact.create(
        userId,
        name,
        homePhone,
        workPhone,
        email,
        address
      );

      await this.contactBookService.createContact(contact);

      this.logger.info("Contact created", { name: contact.name });

      return res.status(200).end();
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  }

  public async update(req: Request, res: Response) {
    const { id } = req.params;
    const { userId, name, homePhone, workPhone, email, address } = req.body;

    try {
      const contact = Contact.create(
        userId,
        name,
        homePhone,
        workPhone,
        email,
        address
      );

      await this.contactBookService.updateContact(id, contact);

      this.logger.info("Contact updated", { name: contact.name });

      return res.status(200).end();
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;

    try {
      await this.contactBookService.deleteContact(id);

      this.logger.info("Contact deleted", { id });

      return res.status(200).end();
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  }

  public async list(req: Request, res: Response) {
    const userId = req.body.userId;
    const { page = 1, limit = 100 } = req.query;

    try {
      const contacts = await this.contactBookService.list(userId, {
        page: Number(page),
        pageSize: Number(limit),
        email: req.query.email as string,
        name: req.query.name as string,
      });

      return res.status(200).json(contacts);
    } catch (error: any) {
      return res.status(400).json({ error: error.message });
    }
  }
}
