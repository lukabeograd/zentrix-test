import AuthenticationService from "../auth/AuthenticationService";
import ContactBookService from "../contactBook/ContactBookService";
import SequelizeConnection from "../database/SequelizeConnection";
import SequelizeAuthenticationRepository from "../database/repositories/SequelizeAuthenticationRepository";
import SequelizeContactBookRepository from "../database/repositories/SequelizeContactBookRepository";
import SequelizeUserRepository from "../database/repositories/SequelizeUserRepository";
import UserService from "../user/UserService";
import WinstonLogger from "../util/logger/WinstonLogger";
import type {
  UtilDependencies,
  RepositoryDependencies,
  ServiceDependencies,
} from "../util/types/dependencies";

export default class DependencyBuilder {
  public static buildUtils(): UtilDependencies {
    const logLevel = process.env.LOG_LEVEL || "info";
    const logFormat = process.env.LOG_FORMAT || "simple";
    const logger = new WinstonLogger(logLevel, logFormat === "json");

    return {
      logger,
    };
  }

  public static buildRepositories(): RepositoryDependencies {
    SequelizeConnection.initiateModels();

    const userRepository = new SequelizeUserRepository();
    const authenticationRepository = new SequelizeAuthenticationRepository();
    const contactBookRepository = new SequelizeContactBookRepository();

    return {
      userRepository,
      authenticationRepository,
      contactBookRepository,
    };
  }

  public static buildServices(
    repositories: RepositoryDependencies
  ): ServiceDependencies {
    const userService = new UserService(repositories.userRepository);
    const authenticationService = new AuthenticationService(
      repositories.authenticationRepository,
      userService
    );
    const contactBookService = new ContactBookService(
      repositories.contactBookRepository
    );

    return {
      userService,
      authenticationService,
      contactBookService,
    };
  }
}
