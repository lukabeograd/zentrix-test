import { Router } from "express";
import AuthController from "../../controllers/AuthController";
import ValidationMiddleware from "../../middlewares/ValidationMiddleware";
import { SignInBodySchema, SignUpBodySchema } from "./Validations";
import type { Dependencies } from "../../../util/types/dependencies";

export default class AuthRouter {
  private constructor() {}

  public static initRoutes(router: Router, deps: Dependencies) {
    const controller = new AuthController(deps);
    router.post(
      "/auth/signup",
      ValidationMiddleware.validate({ body: SignUpBodySchema }),
      controller.signup.bind(controller)
    );
    router.post(
      "/auth/signin",
      ValidationMiddleware.validate({ body: SignInBodySchema }),
      controller.login.bind(controller)
    );
  }
}
