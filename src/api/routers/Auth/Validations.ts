import joi from "joi";

export const SignUpBodySchema = joi
  .object({
    username: joi.string().required(),
    email: joi.string().email().required(),
    password: joi.string().required(),
  })
  .required();

export const SignInBodySchema = joi
  .object({
    email: joi.string().email().required(),
    password: joi.string().required(),
  })
  .required();
