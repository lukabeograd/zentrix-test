import { Router } from "express";
import ValidationMiddleware from "../../middlewares/ValidationMiddleware";
import { ContactBodySchema } from "./Validations";
import type { Dependencies } from "../../../util/types/dependencies";
import ContactBookController from "../../controllers/ContactBookController";
import { DecodeTokenMiddleware } from "../../middlewares/DecodeTokenMiddleware";

export default class ContactBookRouter {
  private constructor() {}

  public static initRoutes(router: Router, deps: Dependencies) {
    const controller = new ContactBookController(deps);
    router.post(
      "/contact/",
      DecodeTokenMiddleware.decodeToken,
      ValidationMiddleware.validate({ body: ContactBodySchema }),
      controller.create.bind(controller)
    );

    router.patch(
      "/contact/:id",
      DecodeTokenMiddleware.decodeToken,
      ValidationMiddleware.validate({ body: ContactBodySchema }),
      controller.update.bind(controller)
    );

    router.delete(
      "/contact/:id",
      DecodeTokenMiddleware.decodeToken,
      controller.delete.bind(controller)
    );

    router.get(
      "/contacts",
      DecodeTokenMiddleware.decodeToken,
      controller.list.bind(controller)
    );
  }
}
