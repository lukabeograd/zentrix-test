import joi from "joi";

export const ContactBodySchema = joi
  .object({
    userId: joi.string().required(),
    name: joi.string().required(),
    homePhone: joi.string().allow(null).optional(),
    workPhone: joi.string().allow(null).optional(),
    email: joi.string().email().allow(null).optional(),
    address: joi.string().allow(null).optional(),
  })
  .required();
