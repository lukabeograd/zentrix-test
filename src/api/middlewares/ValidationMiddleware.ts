import { Request, Response, NextFunction } from "express";
import joi from "joi";

export default class ValidationMiddleware {
  public static validate({
    body,
    query,
    params,
  }: {
    body?: joi.ObjectSchema<any>;
    query?: joi.ObjectSchema<any>;
    params?: joi.ObjectSchema<any>;
  }) {
    return (req: Request, res: Response, next: NextFunction) => {
      if (body) {
        const { error } = body.validate(req.body);
        if (error) {
          return res.status(400).json({ error: error.details[0].message });
        }
      }
      if (query) {
        const { error } = query.validate(req.query);
        if (error) {
          return res.status(400).json({ error: error.details[0].message });
        }
      }
      if (params) {
        const { error } = params.validate(req.params);
        if (error) {
          return res.status(400).json({ error: error.details[0].message });
        }
      }
      next();
    };
  }
}
