import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

export class DecodeTokenMiddleware {
  public static decodeToken(req: Request, res: Response, next: NextFunction) {
    const token = req.headers.authorization?.split(" ")[1];

    if (!token) {
      throw Error("Token not provided");
    }

    try {
      const decodedToken = jwt.decode(token) as { userId: string };

      (req as any).body.userId = decodedToken.userId;

      next();
    } catch (error) {
      throw Error("Token could not be decoded");
    }
  }
}
