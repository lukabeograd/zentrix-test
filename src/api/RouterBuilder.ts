import express from "express";
import AuthRouter from "./routers/Auth/AuthRouter";
import type { Dependencies } from "../util/types/dependencies";
import ContactBookRouter from "./routers/ContactBook/ContactBookRouter";

export default class RouterBuilder {
  public static route(
    app: express.Application,
    dependencies: Dependencies
  ): void {
    const router = express.Router();

    // TODO: Authentication middleware

    AuthRouter.initRoutes(router, dependencies);
    ContactBookRouter.initRoutes(router, dependencies);

    app.use("/api", express.json(), router);

    // TODO: Add health/ready/metrics
  }
}
