import express from "express";
import WebServerConfig from "./WebServerConfig";
import RouterBuilder from "./RouterBuilder";
import DependencyBuilder from "./DependencyBuilder";
import Logger from "../util/logger/Logger";

export default class WebServer {
  private app: express.Application;
  private logger: Logger;

  private constructor(app: express.Application, logger: Logger) {
    this.app = app;
    this.logger = logger;
  }

  public static build(): WebServer {
    const utils = DependencyBuilder.buildUtils();
    const repositories = DependencyBuilder.buildRepositories();
    const services = DependencyBuilder.buildServices(repositories);

    // TODO: Add middlewares
    const app = express();
    const ws = new WebServer(app, utils.logger);

    RouterBuilder.route(app, { ...utils, ...repositories, ...services });

    return ws;
  }

  public run(): void {
    this.app.listen(WebServerConfig.PORT, () => {
      this.logger.info(`Server is running`, { port: WebServerConfig.PORT });
    });
  }
}
