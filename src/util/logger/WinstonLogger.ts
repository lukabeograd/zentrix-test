import Logger from "./Logger";
import winston from "winston";

const JSON_FORMAT = winston.format.combine(
  winston.format.timestamp(),
  winston.format.splat(),
  winston.format.json()
);

const SIMPLE_FORMAT = winston.format.combine(
  winston.format.timestamp(),
  winston.format.colorize(),
  winston.format.simple(),
  winston.format.splat(),
  winston.format.printf((info) => {
    const { timestamp, level, message, ...args } = info;
    const ts = timestamp.slice(0, 19).replace("T", " ");
    return `${ts} [${level}]: ${message} ${
      Object.keys(args).length ? JSON.stringify(args, null) : ""
    }`;
  })
);

export default class WinstonLogger implements Logger {
  private logger: winston.Logger;
  constructor(level: string = "info", json: boolean = false) {
    const logger = winston.createLogger({
      level: level,
      format: json ? JSON_FORMAT : SIMPLE_FORMAT,
      transports: [new winston.transports.Console()],
    });

    this.logger = logger;
  }

  public debug(message: string, meta: Record<string, unknown>): void {
    this.logger.debug(message, meta);
  }

  public info(message: string, meta: Record<string, unknown>): void {
    this.logger.info(message, meta);
  }

  public error(message: string, meta: Record<string, unknown>): void {
    this.logger.error(message, meta);
  }

  public warn(message: string, meta: Record<string, unknown>): void {
    this.logger.warn(message, meta);
  }
}
