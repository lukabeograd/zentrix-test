export default class ListOptions {
  public page: number;
  public pageSize: number;
  public name?: string;
  public email?: string;

  constructor(page: number, pageSize: number, name?: string, email?: string) {
    this.page = page;
    this.pageSize = pageSize;

    this.name = name;
    this.email = email;
  }
}
