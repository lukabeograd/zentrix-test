import * as uuid from "uuid";

export default class Uuid extends String {
  public static create(): string {
    return uuid.v4();
  }
}
