export type UtilDependencies = {
  logger: Logger;
};

export type RepositoryDependencies = {
  userRepository: UserRepository;
  authenticationRepository: AuthenticationRepository;
  contactBookRepository: ContactBookRepository;
};

export type ServiceDependencies = {
  userService: UserService;
  authenticationService: AuthenticationService;
  contactBookService: ContactBookService;
};

export type Dependencies = UtilDependencies &
  RepositoryDependencies &
  ServiceDependencies;
