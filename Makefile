
build:
	yarn build

docker-build:
	docker compose build

docker-run: docker-build
	docker compose up -d 

docker-stop:
	docker compose stop

docker-kill:
	docker compose down -v

docker-reset: 
	docker compose down -v && docker compose build && docker compose up -d