# Description: Dockerfile for the zentrix-test app

# Build image
FROM node:20-alpine as base

# Create app directory
RUN mkdir -p /home/node/zentrix-test && chown -R node:node /home/node/zentrix-test
WORKDIR /home/node/zentrix-test

# Move app files
COPY --chown=node:node package.json yarn.lock tsconfig.json ./
COPY --chown=node:node src/ ./src

USER node

# Install app dependencies
RUN yarn install --frozen-lockfile && \
    yarn build

# Runtime image
FROM node:20-alpine

# Create app directory
RUN mkdir -p /home/node/zentrix-test && chown -R node:node /home/node/zentrix-test
WORKDIR /home/node/zentrix-test

USER node

# Move app files
COPY --chown=node:node package.json yarn.lock ./
COPY --from=base /home/node/zentrix-test/dist ./dist

# Install production dependencies
RUN yarn install --frozen-lockfile --production


ENTRYPOINT ["yarn"]
